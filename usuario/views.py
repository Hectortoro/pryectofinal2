from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Reserva
from sesion.models import Docente
from tutor.models import Tutoria
from django.urls import reverse
from sesion.views import *

# Create your views here.

def index(request):
    return HttpResponse('Inicio')

def usuario(request):
    return render(request, 'usuario/indexUsuario.html')


def  consulta(request):
    return render(request, 'usuario/consultarUsuario.html')

#def  reservar(request):
   # return render(request, 'usuario/reservartutorias.html')

def reservar(request):

    tutor = Docente.objects.all()
    #print(tutor.usuario)
    
    contexto = {'reservar':tutor}

    return render(request, 'usuario/reservartutorias.html', contexto)


def enviar_reserva(request):

    id = int(request.POST['id'])
    docente = Docente.objects.get(pk=id)
        

    tema = request.POST['tema']
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']
    observacion = request.POST['observacion']

    gr = Reserva(tutor=docente, nombre=nombre, apellido=apellido, tema=tema, observacion=observacion)
    gr.save()

    print(gr.tutor.usuario.username)
    print(gr.tema)
    print(gr.observacion)
   



    return HttpResponseRedirect(reverse('usuario'))
    


