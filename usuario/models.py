from django.db import models
from django.contrib.auth.models import User
from sesion.models import Docente


# Create your models here.


class Reserva(models.Model):
    tutor = models.ForeignKey(Docente,
            related_name='usuarios',
            on_delete=models.CASCADE,
            null=False)
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    tema = models.CharField(max_length=200)
    fecha = models.DateTimeField(auto_now_add=True)
    observacion = models.CharField(max_length=200)