from django.urls import path
from . import views

urlpatterns=[
    path('',views.index, name='index'),
    path('principal/', views.usuario, name='usuario'),
    path('principal/consulta', views.consulta, name='consulta'),
    path('principal/registro', views.reservar, name='reservar'),
    #path('principal/re/listatutores', views.lista_tutores, name='tutores')
    path('enviar/', views.enviar_reserva, name='enviar')
]