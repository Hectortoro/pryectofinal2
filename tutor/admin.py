from django.contrib import admin
from .models import Confirma, Tutoria

# Register your models here.

admin.site.register(Confirma)
admin.site.register(Tutoria)