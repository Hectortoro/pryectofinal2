from django.urls import path
from . import views

urlpatterns=[
   # path('',views.index, name='index'),
    path('observarindexa/', views.tutor, name='tutor'),
    path('observarindexa/reportesmensual', views.reportesmensuales, name='reportesmensuales'),
    path('observarindexa/confirmartutoria', views.confirmartutorias, name='confirmartutorias'),
    path('observarindexa/reportesmensual/historialusuario', views.historial, name='historial'),
    path('observarindexa/confirmar', views.confirmar, name='confirmar'),
    path('enviar/', views.enviar_confirmacion, name='enviar_confirmacion'),
    path('observarindexaa/', views.mostrar_tutorias, name='mostrar_tutorias'),
    path('observarindexa/listaUs', views.lista_usuarios_tutoria, name='lista_usuarios_tutoria'),
    path('observarindexa/Historial', views.lista_tutorias, name='lista_tutorias')


]