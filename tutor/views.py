from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Confirma, Tutoria
from usuario.models import Reserva
from sesion.models import Estudiante
from django.urls import reverse

# Create your views here.

#def index(request):
    #return HttpResponse('Inicio')

def tutor(request):
    return render(request, 'tutor/indexadministrador.html')

def  reportesmensuales(request):
    return render(request, 'tutor/reportesmensuales.html')

def  confirmartutorias(request):
    return render(request, 'tutor/confirmartutoria.html')

def  historial(request):
    return render(request, 'tutor/historialUsuario.html')


def confirmar(request):

    confirmarlista = Reserva.objects.all()
   
    
    contexto = {'confirmar':confirmarlista}

    return render(request, 'tutor/confirmartutoria.html', contexto)

def enviar_confirmacion(request):

    id = int(request.POST['id'])
    reserva = Reserva.objects.get(pk=id)
        

    hora = request.POST['hora']
    fecha2 = request.POST['fecha2']
    observaciones = request.POST['contenido']
    
    print(reserva.id)
    gt = Confirma(identificador=reserva, hora=hora, fecha2=fecha2, observaciones=observaciones)
    gt.save()

   # print(gr.tutor.usuario.username)
   # print(gr.tema)
   # print(gr.observacion)

    return HttpResponseRedirect(reverse('tutor'))

def lista_tutorias(request):
    reser = Reserva.objects.all()
    tur = Confirma.objects.all()

    lis = {'reserv':reser,'lis':tur}

    return render(request, 'tutor/historialUsuario.html', lis)

def mostrar_tutorias(request):
    
    id = int(request.POST['id'])
    tutuid = Reserva.objects.get(pk=id)
    observaciones = request.POST['observaciones']

    
    gt = Tutoria(iden=tutuid, observaciones=observaciones)
    gt.save

    return HttpResponseRedirect(reverse('tutor'))

def lista_usuarios_tutoria(request):

    estudiantes = Reserva.objects.all()
    estudiante =  Estudiante.objects.all()
    
    lista_usuarios_tutoria = {'estu':estudiantes, 'estuu':estudiante}

    return render(request, 'tutor/indexadministrador.html', lista_usuarios_tutoria)


