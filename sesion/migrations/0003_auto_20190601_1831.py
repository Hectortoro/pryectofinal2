# Generated by Django 2.2 on 2019-06-01 23:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sesion', '0002_estu_cargo'),
    ]

    operations = [
        migrations.AddField(
            model_name='estu',
            name='apellido',
            field=models.CharField(default=1, max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='estu',
            name='usuario',
            field=models.CharField(default=2, max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='estu',
            name='correo',
            field=models.EmailField(max_length=1200),
        ),
        migrations.AlterField(
            model_name='estu',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
