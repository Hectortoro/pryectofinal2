from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Estu(models.Model):
    id = models.AutoField(primary_key=True) 
    usuario = models.CharField(max_length=200)
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    correo = models.EmailField(max_length=1200)
    codigo = models.CharField(max_length=999999)
    semestre = models.CharField(max_length=15)
    contraseña = models.CharField(max_length=1200)
    #cargo = models.CharField(max_length=1200)

    def _str_(self):
        return self.nombre
    
    class Meta:
        app_label = 'sesion'

class Doc(models.Model):
    id = models.AutoField(primary_key=True) 
    usuario = models.CharField(max_length=200)
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    correo = models.EmailField(max_length=1200)
    codigo = models.CharField(max_length=999999)
    edad = models.CharField(max_length=100)
    contraseña = models.CharField(max_length=1200)
    


class Estudiante(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    codigo = models.CharField(max_length=200)
    semestre = models.CharField(max_length=200)

class Docente(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    carnet = models.CharField(max_length=200)
    edad = models.CharField(max_length=200)

    def __str__(self):
        return self.usuario



        






