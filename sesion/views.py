from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from .models import Estu, Estudiante, Docente
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import User

#def index(request):
 #   return render(request, 'sesion/login.html')


#def  registro(request):
 #   return render(request, 'sesion/registro.html')



def principal(request):
   return render(request, 'home/index.html')

def formulario_login(request):
    
    return render(request, 'sesion/login.html')

def ir_registro(request):

    return render(request, 'sesion/registro.html')

def ir_registroDoc(request):

    return render(request, 'sesion/registroDocente.html')

def hacer_login(request):

    # Obtiene los datos de autenticacion
    usuario = request.POST['usuario']
    password = request.POST['password']

    # Obtiene el usuario con los datos de autenticacion
    user = authenticate(username=usuario, password=password)

    # Autentica el usuario
    if user is not None:
        login(request, user)

        # Redirecciona a la pagina de categorias
        try:
            doc=Docente.objects.get(usuario=user)

            return HttpResponseRedirect(reverse('lista_usuarios_tutoria'))
        except:
            return HttpResponseRedirect(reverse('usuario'))
    else:
        return HttpResponse('<h1>ERROR!!!</h1>')

def hacer_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('principal'))

def crea_cuenta(request):
    valor_usuario = request.POST['usuario']
    valor_nombre = request.POST['nombre']
    valor_apellido = request.POST['apellido']
    valor_correo = request.POST['correo']
    valor_codigo = request.POST['codigo']
    valor_semestre = request.POST['semestre']
    valor_contraseña = request.POST['contraseña']
    #valor_cargo = request.POST['cargo']

    cat = Estu(nombre=valor_nombre, correo=valor_correo, codigo=valor_codigo, semestre=valor_semestre, contraseña=valor_contraseña, usuario=valor_usuario, apellido=valor_apellido )
    cat.save()

    usuarioo=User(
        username=valor_usuario,
        first_name=valor_nombre,
        last_name=valor_apellido,
        email=valor_correo,
        password=valor_contraseña
    )
    usuarioo.set_password(valor_contraseña)
    usuarioo.save()

    estudiante=Estudiante(
        codigo=valor_codigo,
        semestre=valor_semestre,
        usuario=usuarioo
    )
    estudiante.save()

    return HttpResponseRedirect(reverse('principal'))

def registroDocente(request):
    usuarioadmin=request.POST['usuario']
    nombre=request.POST['nombre']
    apellido=request.POST['apellido']
    correo=request.POST['correo']
    codigo=request.POST['codigo']
    edad=request.POST['edad']
    contraseña=request.POST['contraseña']
    #user = authenticate(request, username=usuarioadmin, password=contraseña)
    #if user :
    usuarioo=User(
            username=usuarioadmin,
            first_name=nombre,
            last_name=apellido,
            email=correo,
            password=contraseña
    )
    usuarioo.set_password(contraseña)
    usuarioo.save()
    pr=Docente(
            edad=edad,
            carnet=codigo,
            usuario=usuarioo
    )
    pr.save()
    return HttpResponseRedirect(reverse('principal'))
    #else:
     #   return HttpResponse('No se pudo crear al Docente')