from django.urls import path
from . import views

#urlpatterns=[
 #   path('',views.index, name='index'),
  #  path('registro/', views.registro, name='registro'),

    
#]

urlpatterns = [
    path('', views.principal, name='principal'),
    path('login/', views.formulario_login, name='formulario_login'),
    path('hacer_login/', views.hacer_login, name='login'),
    path('logout/', views.hacer_logout, name='logout'),
    path('registro/', views.ir_registro, name='ir_registro'),
    path('registroDocente/', views.ir_registroDoc, name='ir_registroDoc'),
    path('registroDoc/', views.registroDocente, name='registroDocente'),
    path('crear/', views.crea_cuenta, name='crea_cuenta'),
    
]

